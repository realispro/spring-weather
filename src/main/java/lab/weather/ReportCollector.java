package lab.weather;

import java.time.LocalDateTime;

public interface ReportCollector {
    WeatherReport collect(LocalDateTime dateTime);
}
