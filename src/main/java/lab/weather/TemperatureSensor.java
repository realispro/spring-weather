package lab.weather;

public interface TemperatureSensor {

    float getTemperature();
}
