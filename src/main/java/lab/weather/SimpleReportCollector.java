package lab.weather;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Component("reportCollector")
@Lazy
@Scope("singleton") // prototype
public class SimpleReportCollector implements ReportCollector {

    private TemperatureSensor temperatureSensor;
    private PressureSensor pressureSensor;

    @Autowired
    @Qualifier("services")
    private List<String> services = new ArrayList<>();


    public SimpleReportCollector(
            TemperatureSensor temperatureSensor,
            PressureSensor pressureSensor) {
        this.temperatureSensor = temperatureSensor;
        this.pressureSensor = pressureSensor;
        System.out.println("report collector created.");
    }

    @PostConstruct
    public void init(){
        System.out.println("collector constructed. services: " + services);
    }

    @Override
    public WeatherReport collect(LocalDateTime dateTime){

        System.out.println("collecting weather data at "
                + dateTime.format(DateTimeFormatter.ISO_DATE_TIME));
        System.out.println("collector registered at services " + services);

        return new WeatherReport(dateTime)
                .withTemperature(temperatureSensor.getTemperature())
                .withPressure(pressureSensor.getPressure());
    }

    public TemperatureSensor getTemperatureSensor() {
        return temperatureSensor;
    }


    public void setTemperatureSensor(TemperatureSensor temperatureSensor) {
        this.temperatureSensor = temperatureSensor;
    }

    public PressureSensor getPressureSensor() {
        return pressureSensor;
    }


    public void setPressureSensor(PressureSensor pressureSensor) {
        this.pressureSensor = pressureSensor;
    }

    public List<String> getServices() {
        return services;
    }

    public void setServices(List<String> services) {
        this.services = services;
    }
}
