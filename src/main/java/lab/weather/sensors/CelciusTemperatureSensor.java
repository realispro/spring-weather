package lab.weather.sensors;

import lab.weather.TemperatureSensor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
@Primary
public class CelciusTemperatureSensor implements TemperatureSensor {
    @Override
    public float getTemperature() {
        System.out.println("generating random temp in celcius");
        return new Random().nextFloat();
    }
}
