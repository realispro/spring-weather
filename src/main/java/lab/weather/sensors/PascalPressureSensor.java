package lab.weather.sensors;

import lab.weather.PressureSensor;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component("pressureSensor")
public class PascalPressureSensor implements PressureSensor {
    @Override
    public int getPressure() {
        return new Random(System.currentTimeMillis()).nextInt(100);
    }
}
