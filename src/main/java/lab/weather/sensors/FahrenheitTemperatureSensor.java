package lab.weather.sensors;

import lab.weather.FahrenheitQualifier;
import lab.weather.TemperatureSensor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
@FahrenheitQualifier
public class FahrenheitTemperatureSensor implements TemperatureSensor {
    @Override
    public float getTemperature() {
        return new Random(System.currentTimeMillis()).nextFloat();
    }
}
