package lab.weather;

public interface PressureSensor {

    int getPressure();
}
