package lab.weather;

import lab.weather.config.WeatherConfig;
import lab.weather.sensors.FahrenheitTemperatureSensor;
import lab.weather.sensors.PascalPressureSensor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.time.LocalDateTime;

public class Main {

    public static void main(String[] args) {
        System.out.println("Let's check current weather!");

        ApplicationContext context =
                new AnnotationConfigApplicationContext(WeatherConfig.class);
                //new ClassPathXmlApplicationContext("classpath:context.xml");

        ReportCollector collector = context.getBean(ReportCollector.class);

        WeatherReport report = collector.collect(LocalDateTime.now().plusHours(1));
        System.out.println("Current weather report at [" + report.getTimestamp() + "]" );
        System.out.println("temperature [" + report.getTemperature() + "]");
        System.out.println("pressure [" + report.getPressure() + "]");

        System.out.println("done.");
    }
}
