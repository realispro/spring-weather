package lab.weather.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

import java.util.List;

@Configuration
@ComponentScan("lab.weather")
@PropertySource("classpath:service.properties")
@EnableAspectJAutoProxy
public class WeatherConfig {

    private Environment env;

    @Value("${meteo.service}")
    private String meteoService;

    public WeatherConfig(Environment env) {
        this.env = env;
    }

    @Bean("services")
    List<String> services(){
        //String meteoService = env.getProperty("meteo.service");
        return List.of("meteo.pl", "weather.com", meteoService);
    }

    @Bean
    @Lazy
    String abc(){
        return "abc";
    }

    @Bean
    @Lazy
    String xyz(){
        return "xyz";
    }

}
