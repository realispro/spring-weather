package lab.weather.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Aspect
public class WeatherAspect {

    @Pointcut("execution(public * *(..))")
    public void everything(){}

    @Before("everything()")
    public void logEntering(JoinPoint jp){
        System.out.println("[entering " + jp.toLongString() + "]" + jp.getTarget().getClass().getSimpleName());
    }

    @After("everything()")
    public void logExiting(JoinPoint jp){
        System.out.println("[exiting " + jp.toLongString() + "]" + jp.getTarget().getClass().getSimpleName());
    }

    @Around("execution(public * *(java.time.LocalDateTime))")
    public Object verifyDate(ProceedingJoinPoint jp) throws Throwable {

        System.out.println("around method " + jp.toLongString());
        LocalDateTime dateTime = (LocalDateTime) jp.getArgs()[0];
        if(dateTime.isAfter(LocalDateTime.now())){
            throw new IllegalArgumentException("future date is illegal");
        }
        Object o = jp.proceed(jp.getArgs());
        return o;
    }



}
